import org.junit.*;
import xyz.znix.superblt.Hasher;

import java.io.*;

public class HasherTest {
	@Test
	public void testDirectory() throws IOException {
		String result = Hasher.hashDirectory(new File("../testdata/base"));

		// Test matching the hasher, from the SuperBLT meta
		Assert.assertEquals("77f4d9e3804d345872315a6f0f2a4a19292e50c14a3887f9019fc60bd841a980", result);
	}

	@Test
	public void testFile() throws IOException {
		// To hash a single file:
		// echo -n `sha256sum base.lua | cut -d\  -f1` | sha256sum

		String result = Hasher.hashFile(new File("../testdata/base/base.lua"));

		// Test matching the hasher, from the SuperBLT meta
		Assert.assertEquals("23a9bdaef72002c78411e8976d3d85e73be5c3fd977f78f0de53c6815cbe98e9", result);
	}
}
