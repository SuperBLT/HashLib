package xyz.znix.superblt;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

public class Hasher {

	private Hasher() {
	}

	/**
	 * Hash a directory into a hashstring, as per the BLT hash system.
	 *
	 * @param directory The directory to hash
	 * @return The string-encoded hash, which should exactly match that of BLT's hasher.
	 * @throws IOException If there was an exception reading a file.
	 */
	public static String hashDirectory(File directory) throws IOException {
		StringBuilder contents = new StringBuilder();
		subHashDirectory(directory, contents);
		return HashUtil.formatDigest(HashUtil.sha256(contents.toString().getBytes()));
	}

	/**
	 * Hash a file into a hashstring, as per the BLT hash system.
	 *
	 * @param file The file to hash
	 * @return The string-encoded hash, which should exactly match that of BLT's hasher.
	 * @throws IOException If there was an exception reading a file.
	 */
	public static String hashFile(File file) throws IOException {
		String fileHash = HashUtil.formatDigest(HashUtil.sha256(file));
		return HashUtil.formatDigest(HashUtil.sha256(fileHash.getBytes()));
	}

	/**
	 * Recursively hashes all the files in a directory, putting the relative
	 * paths and hashes into a map.
	 *
	 * @param base   The path relative to which all output keys will appear
	 * @param search The directory to iterate over
	 * @param output The output map
	 * @throws IOException If there was a problem hashing a file
	 */
	private static void hashDirectoryContents(Path base, File search, Map<Path, String> output) throws IOException {
		for (File file : search.listFiles()) {
			if (file.isDirectory()) {
				hashDirectoryContents(base, file, output);
			} else {
				String hash = HashUtil.formatDigest(HashUtil.sha256(file));
				Path relative = base.relativize(file.toPath());
				output.put(relative, hash);
			}
		}
	}

	/**
	 * Iterate through the contents of a directory in alphabetical order,
	 * concatenating the hashes of the files. This method is recursive.
	 *
	 * @param directory The directory to iterate over
	 * @return The concatenated hashes of the contents of this directory. Note
	 * that this is <b>not</b> a hash itself.
	 */
	private static void subHashDirectory(File directory, StringBuilder result) throws IOException {
		Map<Path, String> output = new HashMap<>();
		hashDirectoryContents(directory.toPath(), directory, output);
		output.keySet().stream()
				.sorted((a, b) -> a.toString().compareToIgnoreCase(b.toString()))
				.peek(System.out::println)
				.map(output::get)
				.forEachOrdered(result::append);
	}

}
